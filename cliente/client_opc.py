from opcua import Client, ua
import system_functions
import psycopg2


DB_CONNECTION = "postgres://postgres:password@timescale:5432/SMCmodel"
CLIENT_URL = 'opc.tcp://10.200.102.10:4840'
CLIENT_USER = ''
CLIENT_PASS = ''
AUTO_1_2_ID = 'ns=3;s="MARCHA-PARO(1-2)"'
AUTO_2_1_ID = 'ns=3;s="MARCHA-PARO(2-1)"'
AUX_TANK_ID = 'ns=3;s="LLENADO DE AUX"'
MAX1_ID = 'ns=3;s="max1"'
MAX2_ID = 'ns=3;s="max2"'
MIN1_ID = 'ns=3;s="min1"'
MIN2_ID = 'ns=3;s="min2"'
B_1_ID ='ns=3;s="B1+"'
B_2_ID ='ns=3;s="B2+"'
B_3_ID ='ns=3;s="B3+"'
A_1_ID ='ns=3;s="A1+"'
A_2_ID ='ns=3;s="A2+"'
A_3_ID ='ns=3;s="A3PLUS"'
A_4_ID ='ns=3;s="A4+"'
A_5_ID ='ns=3;s="A5+"'


class OPCUAHandler(object):
	def __init__(self, model):
		self.model = model
	def datachange_notification(self, node, val, data):
		self.model.clasify_variables(node, val)

class OPCUA_Client:
	def __init__(self):

		self.client = Client(CLIENT_URL)
		self.client.set_user(CLIENT_USER)
		self.client.set_password(CLIENT_PASS)
		self.client.connect()

		self.auto_1_2 = self.client.get_node(AUTO_1_2_ID)
		self.auto_2_1 = self.client.get_node(AUTO_2_1_ID)
		self.aux_tank = self.client.get_node(AUX_TANK_ID)
		self.max_1 = self.client.get_node(MAX1_ID)
		self.max_2 = self.client.get_node(MAX2_ID)
		self.min_1 = self.client.get_node(MIN1_ID)
		self.min_2 = self.client.get_node(MIN2_ID)
		self.b_1 = self.client.get_node(B_1_ID)
		self.b_2 = self.client.get_node(B_2_ID)
		self.b_3 = self.client.get_node(B_3_ID)
		self.a_1 = self.client.get_node(A_1_ID)
		self.a_2 = self.client.get_node(A_2_ID)
		self.a_3 = self.client.get_node(A_3_ID)
		self.a_4 = self.client.get_node(A_4_ID)
		self.a_5 = self.client.get_node(A_5_ID)

		self.handler_data = OPCUAHandler(self)
		sub_data = self.client.create_subscription(500, self.handler_data)
		handle_data = sub_data.subscribe_data_change(self.min_1)
		handle_data = sub_data.subscribe_data_change(self.min_2)
		handle_data = sub_data.subscribe_data_change(self.max_1)
		handle_data = sub_data.subscribe_data_change(self.max_2)	
		handle_data = sub_data.subscribe_data_change(self.b_1)
		handle_data = sub_data.subscribe_data_change(self.b_2)
		handle_data = sub_data.subscribe_data_change(self.b_3)
		handle_data = sub_data.subscribe_data_change(self.a_1)
		handle_data = sub_data.subscribe_data_change(self.a_2)
		handle_data = sub_data.subscribe_data_change(self.a_3)
		handle_data = sub_data.subscribe_data_change(self.a_4)
		handle_data = sub_data.subscribe_data_change(self.a_5)

		self.max_1_val = self.max_1.get_value()
		self.max_2_val = self.max_2.get_value()
		self.min_1_val = self.min_1.get_value()
		self.min_2_val = self.min_2.get_value()
		self.B_1_val = self.b_1.get_value() 
		self.B_2_val = self.b_2.get_value()
		self.B_3_val = self.b_3.get_value()
		self.A_1_val = self.a_1.get_value()
		self.A_2_val = self.a_2.get_value()
		self.A_3_val = self.a_3.get_value()
		self.A_4_val = self.a_4.get_value()
		self.A_5_val = self.a_5.get_value()

		self.left_fill_time = 0
		self.right_fill_time = 0
		self.max_level = 0
		self.min_level = 0
		self.measure_time = 0
		self.model_state = 'prepare'
        
	def fill_left_tank(self):
		self.auto_2_1.set_value(ua.DataValue(ua.Variant(1, ua.VariantType.Boolean)))
		self.auto_2_1_val = True

	def stop_fill_left_tank(self):
		self.auto_2_1.set_value(ua.DataValue(ua.Variant(0, ua.VariantType.Boolean)))
		self.auto_2_1_val = False

	def fill_right_tank(self):
		self.auto_1_2.set_value(ua.DataValue(ua.Variant(1, ua.VariantType.Boolean)))
		self.auto_1_2_val = True
        
	def stop_fill_right_tank(self):
		self.auto_1_2.set_value(ua.DataValue(ua.Variant(0, ua.VariantType.Boolean)))
		self.auto_1_2_val = False

	def open_aux_tank(self):
		self.aux_tank.set_value(ua.DataValue(ua.Variant(1, ua.VariantType.Boolean)))
		self.aux_tank_val = True

	def close_aux_tank(self):
		self.aux_tank.set_value(ua.DataValue(ua.Variant(0, ua.VariantType.Boolean)))
		self.aux_tank_val = False

	def get_min1(self):
		if self.min_1_val == False:
			return True
		else:
			return False
    
	def get_min2(self):
		if self.min_2_val == False:
			return True
		else:
			return False
    
	def get_max1(self):
		if self.max_1_val == False:
			return True
		else:
			return False
    
	def get_max2(self):
		if self.max_2_val == False:
			return True
		else:
			return False

	def clasify_variables(self, node, val):
		if str(node) == 'ns=3;s="max1"':
			self.max_1_val = val
		elif str(node) == 'ns=3;s="max2"':
			self.max_2_val = val
		elif str(node) == 'ns=3;s="min1"':
			self.min_1_val = val
		elif str(node) == 'ns=3;s="min2"':
			self.min_2_val = val
		elif str(node) == 'ns=3;s="B1+"':
			self.B_1_val = val 
		elif str(node) == 'ns=3;s="B2+"':
			self.B_2_val = val
		elif str(node) == 'ns=3;s="B3+"':
			self.B_3_val = val
		elif str(node) == 'ns=3;s="A1+"':
			self.A_1_val = val
		elif str(node) == 'ns=3;s="A2+"':
			self.A_2_val = val
		elif str(node) == 'ns=3;s="A3PLUS"':
			self.A_3_val = val
		elif str(node) == 'ns=3;s="A4+"':
			self.A_4_val = val
		elif str(node) == 'ns=3;s="A5+"':
			self.A_5_val = val
		self.insert_model_info()	
			
	def insert_model_info(self):
		conn = psycopg2.connect(DB_CONNECTION)
		cursor = conn.cursor()
		cursor.execute("INSERT INTO model_info (actual_time, state, B1, B2, B3, A1, A2, A3, A4, A5, min1, min2, max1, max2) VALUES(current_timestamp, '"+str(self.model_state)+"', '"+str(self.B_1_val)+"', '"+str(self.B_2_val)+"', '"+str(self.B_3_val)+"', '"+str(self.A_1_val)+"', '"+str(self.A_2_val)+"', '"+str(self.A_3_val)+"', '"+str(self.A_4_val)+"', '"+str(self.A_5_val)+"', '"+str(self.min_1_val)+"', '"+str(self.min_1_val)+"', '"+str(self.max_1_val)+"', '"+str(self.max_2_val)+"')")
		conn.commit()
		cursor.close()

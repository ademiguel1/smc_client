from transitions import Machine

STATE_LIST=['prepare', 'save_data', 'restore', 'fill_left', 'fill_right', 'finale', 'recovery', 'warning']

class Model_State_Machine:
	def __init__(self):
		self.machine = Machine(model=self, states=STATE_LIST, initial='prepare')

		self.machine.add_transition(trigger='go_right', source=['fill_left','prepare'], dest='fill_right')
		self.machine.add_transition(trigger='save', source='fill_right', dest='save_data')
		self.machine.add_transition(trigger='go_left', source=['save_data','prepare', 'fill_right'], dest='fill_left')
		self.machine.add_transition(trigger='alert', source='*', dest='warning')
		
		self.machine.add_transition(trigger='recover', source='warning', dest='recovery')
		self.machine.add_transition(trigger='restart', source=['recovery','save_data', 'fill_left', 'fill_right'], dest='prepare')
		
		self.machine.add_transition(trigger='end', source='*', dest='finale')
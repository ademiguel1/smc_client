import client_opc
import state_machine
import time
import psycopg2

DB_CONNECTION = "postgres://postgres:password@timescale:5432/SMCmodel"

client = client_opc.OPCUA_Client()
machine = state_machine.Model_State_Machine()

def get_actual_time():
	return time.time()

def wait_for_security():
	time.sleep(2)

def fill_tank(tank, stop_fill):
	end = True
	fill_loop = True
	while fill_loop:
		if tank == '1':
			client.fill_left_tank()
			machine.go_left()
			client.model_state = machine.state
			client.measure_time = get_actual_time()
			while end:
				if stop_fill.stop_value == 1:
					end = False
					fill_loop = False
					client.stop_fill_left_tank()
					machine.restart()
					client.model_state = machine.state
					client.left_fill_time = get_actual_time() - client.measure_time

		elif tank == '2':
			client.fill_right_tank()
			machine.go_right()
			client.model_state = machine.state
			client.measure_time = get_actual_time()
			while end:
				if stop_fill.stop_value == 1:
					end = False
					fill_loop = False
					client.stop_fill_right_tank()
					machine.restart()
					client.model_state = machine.state
					client.right_fill_time = get_actual_time() - client.measure_time
		elif tank == '3':
			return True
		else:
			print('Wrong data')
			fill_loop = False


def auto_function(stop_auto):
	auto_loop = True
	init_time = get_actual_time()
	pause_time_rest = 0
	pause_control = 0
	left_work_time = client.left_fill_time
	right_work_time = client.right_fill_time
	while auto_loop:
		if stop_auto.stop_value == 1:
			client.stop_fill_left_tank()
			client.stop_fill_right_tank()
			auto_loop = False
			machine.restart()
			client.model_state = machine.state
			# Insert data to DB
		if stop_auto.pause_value == 1:
			if pause_control == 0:
				if machine.state == 'fill_right':
					right_work_time = right_work_time - (get_actual_time() - client.measure_time)
					client.stop_fill_right_tank()
				elif machine.state == 'fill_left':
					left_work_time = left_work_time - (get_actual_time() - client.measure_time)
					client.stop_fill_left_tank()
				pause_control = 1
			
		else:
			if client.get_max1 == True:
				client.stop_fill_left_tank()
				machine.alert()
				client.model_state = machine.state
			if client.get_max2 == True:
				client.stop_fill_right_tank()
				machine.alert()
				client.model_state = machine.state
			if client.get_min1 == True:
				client.stop_fill_right_tank()
				machine.alert()
				client.model_state = machine.state
			if client.get_min2 == True:
				client.stop_fill_left_tank()
				machine.alert()
				client.model_state = machine.state
			elif machine.state == 'prepare':
				control = 0
				machine.go_left()
				client.model_state = machine.state
			elif machine.state == 'fill_left':
				if control == 0:
					client.fill_left_tank()
					client.measure_time = get_actual_time()
					control = 1
					pause_time_rest = 0
				elif control == 1:
					if pause_control == 1:
						client.fill_left_tank()
						client.measure_time = get_actual_time()
						pause_control = 0
					if (get_actual_time() - client.measure_time) > left_work_time:
						client.stop_fill_left_tank()
						wait_for_security()	
						control = 0
						machine.go_right()
						client.model_state = machine.state
						wait_for_security()
						pause_time_rest = 0
						left_work_time = client.left_fill_time
			elif machine.state == 'fill_right':
				if control == 0:
					client.fill_right_tank()
					client.measure_time = get_actual_time()
					control = 1
					pause_time_rest = 0
				elif control == 1:
					if pause_control == 1:
						client.fill_right_tank()
						client.measure_time = get_actual_time()
						pause_control = 0
					if (get_actual_time() - client.measure_time) > right_work_time:
						client.stop_fill_right_tank()
						wait_for_security()
						control = 0
						machine.go_left()
						client.model_state = machine.state
						wait_for_security()
						pause_time_rest = 0
						right_work_time = client.right_fill_time
				elif machine.state == 'warning':
					machine.restart()
					client.model_state = machine.state
					
		

def store_max_min_levels(fase, level):
	if fase == 'max':
		client.max_level = level
	elif fase == 'min':
		client.min_level = level

def calculate_theorical_level():
	if machine.state == 'fill_right':
		return ((get_actual_time() - client.measure_time)*(client.max_level - client.min_level))/(client.right_fill_time) + client.min_level
	elif machine.state == 'fill_left':
		return ((get_actual_time() - client.measure_time)*(client.max_level - client.min_level))/(client.left_fill_time) + client.min_level

def insert_level_info(real):
	conn = psycopg2.connect(DB_CONNECTION)
	cursor = conn.cursor()
	cursor.execute("INSERT INTO levels (actual_time, theorical_level, real_level) VALUES(current_timestamp, '"+str(calculate_theorical_level())+"', '"+str(real)+"')")
	conn.commit()
	cursor.close()

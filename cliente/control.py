import system_functions
from threading import Thread

fase = 0
program = 0
loop_client = True
loop_program_1 = True
loop_program_2 = True
control = 0
measure_time = 0
left_fill_time = 0
right_fill_time = 0
button = 0
tank = 0
pause_function = 0
pause_auto_function = 0
stop_auto_function = 0
total_work_time = 0
auto_function_loop = True

class Stop_class:
	def __init__(self):
		self.stop_value = 0
		self.pause_value = 0

while loop_client:
	stop_c = Stop_class()
	
	print('#################################################')
	print('#     Enter [1] key to init calibration         #')
	print('#     Enter [2] key to fill tanks manually      #')
	print('#     Enter [3] key to exit                     #')
	print('#################################################')
	program = input()
            
	if program == '1':
		loop_program_1 = True
		while loop_program_1:
			if fase == 0:
				print('------------------------------')
				print('-    Calibration selected    -')
				print('------------------------------')
				print(' ')
				print('... Empty the left tank to the limit ...')
				print(' ')
				tank = '2'
				stop_c.stop_value = 0
				calibration_thread = Thread(target=system_functions.fill_tank, args=[tank, stop_c])
				calibration_thread.start()

				print('Press [s] to stop')
				print(' ')
				button = input()
				print(' ')
				if button == 's':
					stop_c.stop_value = 1
					fase = 1
					calibration_thread.join()

			elif fase == 1:
				print('... Measuring left tank ...')
				print(' ')
				tank = '1'
				stop_c.stop_value = 0
				system_functions.wait_for_security()
				calibration_thread = Thread(target=system_functions.fill_tank, args=[tank, stop_c])
				calibration_thread.start()

				print('Enter [s] to stop when the left tank is full')
				print(' ')
				button = input()
				print(' ')
				if button == 's':
					stop_c.stop_value = 1
					fase = 2
					calibration_thread.join()
					system_functions.wait_for_security()
					print('------------------------------')
					print('-    Insert actual level:    -')
					print('------------------------------')
					print(' ')
					button = input()
					print(' ')
					system_functions.store_max_min_levels('min', float(button))


			elif fase == 2:
				print('... Measuring right tank ...')
				print(' ')
				tank = '2'
				stop_c.stop_value = 0
				
				calibration_thread = Thread(target=system_functions.fill_tank, args=[tank, stop_c])
				calibration_thread.start()

				print('Enter [s] to stop when the right tank is full')
				print(' ')
				button = input()
				print(' ')
				if button == 's':
					stop_c.stop_value = 1
					fase = 3
					calibration_thread.join()
					system_functions.wait_for_security()
					print('------------------------------')
					print('-    Insert actual level:    -')
					print('------------------------------')
					print(' ')
					button = input()
					print(' ')
					system_functions.store_max_min_levels('max', float(button))


			elif fase == 3:
				print('####################################################')
				print('#     Enter [1] key to manipulate tanks manually   #')
				print('#     Enter [2] key to init auto function          #')
				print('#     Enter [3] key to exit                        #')
				print('####################################################')
				print(' ')
				selection = input()
				print(' ')

				if selection == '1':
					fase = 4
				elif selection == '2':
					fase = 5
				elif selection == '3':
					loop_program_1 = False
					loop_client = False

			elif fase == 4:
				print('-------------------------------------')
				print('-    Press [1] to fill left tank    -')
				print('-    Press [2] to fill right tank   -')
				print('-    Press [3] to quit              -')
				print('-------------------------------------')
				print(' ')
				tank = input()
				print(' ')
				stop_c.stop_value = 0
				manual_use_thread = Thread(target=system_functions.fill_tank, args=[tank, stop_c])
				manual_use_thread.start()
				finish_manual_manipulation = True
				while finish_manual_manipulation:
					print('Enter [s] to stop the manual manipulation')
					print(' ')
					button = input()
					print(' ')
					if button == 's':
						stop_c.stop_value = 1
						finish_manual_manipulation = False
						fase = 3
						manual_use_thread.join()

			elif fase == 5:
				print('--------------------------------')
				print('-    Auto function selected    -')
				print('--------------------------------')
				print(' ')
				stop_c.pause_value = 0
				stop_c.stop_value = 0
				total_work_time = 0
				auto_function_thread = Thread(target=system_functions.auto_function, args=[stop_c])
				auto_function_thread.start()
				auto_function_loop = True
				while auto_function_loop:
					print('---------------------------------------------------')
					print('-    Press [1] to pause and enter level values    -')
					print('-    Press [2] to stop auto function              -')
					print('---------------------------------------------------')
					print(' ')
					button = input()
					print(' ')
					if button == '1':
						stop_c.pause_value = 1
						system_functions.wait_for_security()
						print('------------------------------')
						print('-    Insert actual level:    -')
						print('------------------------------')
						print(' ')
						button = input()
						print(' ')
						actual_level = float(button)
						system_functions.insert_level_info(actual_level)
						system_functions.wait_for_security()
						stop_c.pause_value = 0
					elif button == '2':
						stop_c.stop_value = 1
						system_functions.wait_for_security()
						auto_function_loop = False
						fase = 3
						auto_function_thread.join()




	elif program == '2':
		loop_program_2 = True
		while loop_program_2:
			print('-------------------------------------')
			print('-    Press [1] to fill left tank    -')
			print('-    Press [2] to fill right tank   -')
			print('-    Press [3] to quit              -')
			print('-------------------------------------')
			print(' ')
			tank = input()
			print(' ')
			if tank != '3':
				stop_c.stop_value = 0
				manual_use_thread = Thread(target=system_functions.fill_tank, args=[tank, stop_c])
				manual_use_thread.start()
				finish_manual_manipulation = True
				while finish_manual_manipulation:
					print('Enter [s] to stop the manual manipulation')
					print(' ')
					button = input()
					print(' ')
					if button == 's':
						stop_c.stop_value = 1
						finish_manual_manipulation = False
						loop_program_2 = False
						manual_use_thread.join()
			else:
				loop_program_2 = False
				
	elif program == '3':
		loop_client = False
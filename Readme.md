# Cliente de la maqueta SMC

Este cliente basado en OPC UA ha sido diseñado para poner en marcha la maqueta SMC de manera autonoma y sacar datos para luego hacer un tratamiento sobre ellas.

## Funcionamiento autonomo

Al iniciar el programa, se daran 2 opciones de uso:
* Calibracion
* Movimiento manual

### Calibracion

En este modo, el sistema te pedira que lleves los tanques al limite para medir el tiempo que tarda cada bomba en llenar cada tanque.
Una vez se alla calibrado, el sistema te dara la opcion de manejar las bombas manualmente o poner en marcha el sistema autonomamente.

### Movimiento manual

En este modo, el sistema nos dejara llenar o vaciar los tanques de manera manual.

### Funcionamiento autonomo

El funcionamiento autonomo funcionara llevando cada uno de los tanques a sus limites usando los tiempos que se han conseguido en la calibracion. En cualquier momento se podra pausar la funcion para comparar el valor real del nivel de los tanques con los teoricos calculados a base de los timepos de calibracion. Tambien se podra apagar la funcion en cualquier momento.

## Ponerlo en marcha

Para poner en marcha el sistema, primero hay que llamar al docker compose para iniciar la base de datos, y el visualizador de datos. 

Una vez el docker esta corriendo, abrir una terminal dentro del docker del cliente y correr el control.py que esta en el directorio de inicio.
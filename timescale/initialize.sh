psql --username "$POSTGRES_USER" <<EOF
CREATE DATABASE SMCmodel WITH OWNER $POSTGRES_USER;
GRANT ALL PRIVILEGES ON DATABASE SMCmodel TO $POSTGRES_USER;

\c SMCmodel
CREATE EXTENSION IF NOT EXISTS timescaledb CASCADE;


CREATE TABLE model_info (actual_time TIMESTAMP, state VARCHAR(20), B1 BOOLEAN, B2 BOOLEAN, B3 BOOLEAN, A1 BOOLEAN, A2 BOOLEAN, A3 VARCHAR(5), A4 BOOLEAN, A5 BOOLEAN, min1 BOOLEAN, min2 BOOLEAN, max1 BOOLEAN, max2 BOOLEAN);
SELECT create_hypertable('model_info', 'actual_time');
CREATE TABLE levels (actual_time TIMESTAMP, theorical_level FLOAT, real_level FLOAT);
SELECT create_hypertable('levels', 'actual_time');
EOF